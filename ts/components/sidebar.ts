let $sidebar: JQuery = $(".sidebar");

$sidebar
    .find("[data-toggle=collapse]")
    .append("<i class='fas fa-caret-right fa-lg float-right'></i>");