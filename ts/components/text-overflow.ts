import "bootstrap/js/dist/tooltip";

$(".text-overflow").on("mouseenter", (e: any) => {
    let $this: JQuery = $(e.target);
    let $span = $("<span></span>")
        .appendTo('body')
        .text($this.text())
        .css({
            "font-size": $this.css("font-size"),
            "display": "none"
        });
    if ($span.width() > $this.width()) {
        $this.tooltip({
            title: $this.text(),
            trigger: "hover"
        }).tooltip("show");
    }
    $span.remove();
});